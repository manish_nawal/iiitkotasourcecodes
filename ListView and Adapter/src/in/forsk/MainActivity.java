package in.forsk;

import in.forsk.adapter.FacultyListAdapter;
import in.forsk.wrapper.FacultyWrapper;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONObject;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.widget.ListView;

public class MainActivity extends Activity {

	public final static String TAG = MainActivity.class.getSimpleName();
	private Context context;

	ListView mFacultyList;
	FacultyListAdapter mAdapter;

	// Collection Framework
	ArrayList<FacultyWrapper> mFacultyDataList = new ArrayList<FacultyWrapper>();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		context	=	 this;
		
		mFacultyList = (ListView) findViewById(R.id.facultyList);
		
		parseRemoteFacultyFile("http://iiitkota.forsklabs.in/faculty_profile.txt");
	}

	public void parseRemoteFacultyFile(final String url) {
		new FacultyParseAsyncTask().execute(url);
	}

	// Params, Progress, Result
	class FacultyParseAsyncTask extends AsyncTask<String, Integer, String> {

		ProgressDialog pd;

		@Override
		protected void onPreExecute() {
			super.onPreExecute();

			pd = new ProgressDialog(context);
			pd.setMessage("loading..");
			pd.setCancelable(false);
			pd.show();
		}

		@Override
		protected String doInBackground(String... params) {
			String url = params[0];
			InputStream is = openHttpConnection(url);
			String response = "";
			try {
				response = convertStreamToString(is);

				onProgressUpdate(0);
			} catch (IOException e) {
				e.printStackTrace();
			}
			return response;
		}

		@Override
		protected void onProgressUpdate(Integer... values) {
			// TODO Auto-generated method stub
			super.onProgressUpdate(values);
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			ArrayList<FacultyWrapper> mFacultyDataList = pasreLocalFacultyFile(result);
			setFacultyListAdapter(mFacultyDataList);

			if (pd != null)
				pd.dismiss();
		}
	}

	private InputStream openHttpConnection(String urlStr) {
		InputStream in = null;
		int resCode = -1;

		try {
			URL url = new URL(urlStr);
			URLConnection urlConn = url.openConnection();

			if (!(urlConn instanceof HttpURLConnection)) {
				throw new IOException("URL is not an Http URL");
			}

			HttpURLConnection httpConn = (HttpURLConnection) urlConn;
			httpConn.setAllowUserInteraction(false);
			httpConn.setInstanceFollowRedirects(true);
			httpConn.setRequestMethod("GET");
			httpConn.connect();

			resCode = httpConn.getResponseCode();
			if (resCode == HttpURLConnection.HTTP_OK) {
				in = httpConn.getInputStream();
			}
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return in;
	}

	public void setFacultyListAdapter(ArrayList<FacultyWrapper> mFacultyDataList) {
		mAdapter = new FacultyListAdapter(context, mFacultyDataList);
		mFacultyList.setAdapter(mAdapter);
	}

	public ArrayList<FacultyWrapper> pasreLocalFacultyFile(String json_string) {

		ArrayList<FacultyWrapper> mFacultyDataList = new ArrayList<FacultyWrapper>();
		try {
			// Converting multipal json data (String) into Json array
			JSONArray facultyArray = new JSONArray(json_string);
			Log.d(TAG, facultyArray.toString());
			// Iterating json array into json objects
			for (int i = 0; facultyArray.length() > i; i++) {

				// Extracting json object from particular index of array
				JSONObject facultyJsonObject = facultyArray.getJSONObject(i);

				// Design patterns
				FacultyWrapper facultyObject = new FacultyWrapper(facultyJsonObject);

				printObject(facultyObject);

				mFacultyDataList.add(facultyObject);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return mFacultyDataList;
	}

	public void printObject(FacultyWrapper obj) {
		// Operator Overloading
		Log.d(TAG, "First Name : " + obj.getFirst_name());
		Log.d(TAG, "Last Name : " + obj.getLast_name());
		Log.d(TAG, "Photo : " + obj.getPhoto());
		Log.d(TAG, "Department : " + obj.getDepartment());
		Log.d(TAG, "reserch_area : " + obj.getReserch_area());
		Log.d(TAG, "Phone : " + obj.getPhone());
		Log.d(TAG, "Email : " + obj.getEmail());

		for (String s : obj.getInterest_areas()) {
			Log.d(TAG, "Interest Area : " + s);
		}
	}

	private String convertStreamToString(InputStream is) throws IOException {
		// Converting input stream into string
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		int i = is.read();
		while (i != -1) {
			baos.write(i);
			i = is.read();
		}
		return baos.toString();
	}

}
